<!DOCTYPE html>
<html>
<head>
	<title>Form</title>
</head>
<body>

<h2>Buat Account Baru!</h2>
<h4>Sign Up Form</h4>

<form action="/selamatdatang" method="POST">
@csrf
<div>
	First name: <br />
	<br />
	<input type="text" name="namadepan" required placeholder="masukkan nama depan" /><br />
	<br />

	Last name: <br />
	<br />
	<input type="text" name="namabelakang" required placeholder="masukkan nama belakang" /><br />
	<br />
</div>

<div>
	Gender: <br />
	<br />

	<input type="radio" name="kelamin" value="male" />
	Male
	<br />

	<input type="radio" name="kelamin" value="female" />
	Female
	<br />

	<input type="radio" name="kelamin" value="other" />
	Other
</div>

<div>
	<br />
	Nationality: <br />
	<br />

	<select name="kewarganegaraan">
		<option value="indonesian">Indonesian</option><br />
		<br />
		
		<option value="singaporean">Singaporean</option><br />
		<br />
		
		<option value="italian">Italian</option><br />
		<br />
		
		<option value="australian">Australian</option>
	</select>
</div>
	
<div>
	<br />
	Language Spoken: <br />
	<br />

	<input type="checkbox" name="bahasaindonesia" />
	Bahasa Indonesia
	<br />

	<input type="checkbox" name="english" />
	English
	<br />

	<input type="checkbox" name="other" />
	Other
</div>

<div>
	<br />
	Bio: <br />
	<br />
	<textarea name="bio" rows="11" cols="33">
		
	</textarea>
	<br />

</div>

	<input type="submit" value="Sign Up" />

</form>

</body>
</html>
