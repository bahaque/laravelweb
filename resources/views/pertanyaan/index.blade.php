@extends('adminlte.master');

@section('content')
    <div class="mt-3 ml-3">
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Questions Table</h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
        @if(session('success'))
            <div class="alert alert-success">
                {{ session('success') }}
            </div>
        @endif

        <!-- Cara Query Builder -->
        <!-- <a class="btn btn-primary mb-3" href="/pertanyaan/create">Create New Post</a> -->
        
        <!-- Cara Eloquent -->
        <a class="btn btn-primary mb-3" href="{{ route('pertanyaan.create') }}">Create New Post</a>

            <table class="table table-bordered">
                <thead>                  
                    <tr>
                        <th style="width: 10px">#</th>
                        <th>Title</th>
                        <th>Body</th>
                        <th style="width: 40px">Action</th>
                    </tr>
                </thead>
                <tbody>
                <!-- object = $post . property = title -->
                    @forelse($posts as $key => $post) 
                    <tr>
                        <td> {{ $key + 1 }} </td>
                        <td> {{ $post->title }} </td>
                        <td> {{ $post->body }} </td>
                        <td>
                            <!-- <a href="/pertanyaan/{{$post->id}}" class="btn btn-info btn-sm">show</a> -->
                            <a href="{{ route('pertanyaan.show', ['pertanyaan' => $post->id]) }}" class="btn btn-info btn-sm">show</a>
                            
                            <!-- <a href="/pertanyaan/{{$post->id}}/edit" class="btn btn-info btn-sm ml-2">edit</a> -->
                            <a href="{{ route('pertanyaan.edit', ['pertanyaan' => $post->id]) }}" class="btn btn-info btn-sm ml-2">edit</a>
                            
                            <!-- <form action="/pertanyaan/{{$post->id}}" method="post"> -->
                            <form action="{{ route('pertanyaan.destroy', ['pertanyaan' => $post->id]) }}" method="post">
                            @csrf
                            @method('DELETE')
                                <input type="submit" value="delete" class="btn btn-danger btn-sm mt-2">
                            </form>
                        </td>
                    </tr>
                    @empty
                    <tr>
                        <td colspan="4" align="center"> No Posts </td>
                    </tr>
                    @endforelse
                </tbody>
            </table>
            </div>
            <!-- /.card-body -->
            <!-- <div class="card-footer clearfix">
                <ul class="pagination pagination-sm m-0 float-right">
                    <li class="page-item"><a class="page-link" href="#">«</a></li>
                    <li class="page-item"><a class="page-link" href="#">1</a></li>
                    <li class="page-item"><a class="page-link" href="#">2</a></li>
                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                    <li class="page-item"><a class="page-link" href="#">»</a></li>
                </ul>
            </div> -->
    </div>
    </div>
@endsection