<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pertanyaan extends Model
{
    // cara Eloquent
    protected $table = "questions";

    // cara Mass Assignment
    // protected $fillable = ["title", "body"];

    // Cara Guarded -> kebalikan dr Mass Ass.
    protected $guarded = []; // yg di blacklist kosong atau semua tabel diisi
}
