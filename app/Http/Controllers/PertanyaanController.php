<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Pertanyaan;

class PertanyaanController extends Controller
{
    public function create() {
        // menampilkan form untuk membuat pertanyaan baru
        return view('pertanyaan.create');
    }

    public function store(Request $request) {
        // menyimpan data baru ke tabel pertanyaan
        // dd($request->all());
        $request->validate([
            'title' => 'required|unique:questions',
            'body' => 'required'
        ]);
        // Cara Eloquent
        // $pertanyaan = objek baru dr kelas Pertanyaan
        // $pertanyaan = new Pertanyaan;
        // $pertanyaan->title = $request["title"];
        // $pertanyaan->body = $request["body"];
        // $pertanyaan->save(); // insert into pertanyaan (title, body) values

        // $query = DB::table('questions')->insert([
        //     'title' => $request['title'],
        //     'body' => $request['body']
        // ]);
        
        // cara Mass Assignment => semua kolom pd tabel dijabarin satu2 seperti ini :
        $pertanyaan = Pertanyaan::create([
            "title" => $request["title"],
            "body" => $request["body"]
        ]);

        return redirect('/pertanyaan')->with('success', 'Post Berhasil Disimpan!');
    }

    public function index() {
        // menampilkan list data pertanyaan-pertanyaan 
        // (boleh menggunakan table html atau bootstrap card)
        // $posts = DB::table('questions')->get();
        // dd($posts);

        
        // Eloquent
        $posts = Pertanyaan::all();
        return view('pertanyaan.index', compact('posts')); // compact('variabel')
    }

    public function show($id) {
        // menampilkan detail pertanyaan dengan id tertentu
        // $post = DB::table('questions')->where('id', $id)->first();

        // Eloquent
        $post = Pertanyaan::find($id); // bs pakek find atau where. Baca di laravel.com
        // dd($post);
        return view('pertanyaan.show', compact('post'));
    }

    public function edit($id) {
        // menampilkan form untuk edit pertanyaan dengan id tertentu
        // $post = DB::table('questions')->where('id', $id)->first();
        
        // Eloquent
        $post = Pertanyaan::find($id);
        
        return view('pertanyaan.edit', compact('post'));
    }

    public function update($id, Request $request) {
        // menyimpan perubahan data pertanyaan (update) untuk id tertentu
        // $request->validate([
        //     'title' => 'required|unique:questions',
        //     'body' => 'required'
        // ]);

        // $query = DB::table('questions')
        //         ->where('id', $id)
        //         ->update([
        //             'title' => $request['title'],
        //             'body' => $request['body']
        //         ]);

        // Cara Eloquent
        $update = Pertanyaan::where('id', $id)->update([
            "title" => $request["title"],
            "body" => $request["body"]
        ]);
        
        return redirect('/pertanyaan')->with('success', "Berhasil Update Post!");
    }

    public function destroy($id) {
        // menghapus pertanyaan dengan id tertentu
        // $query = DB::table('questions')->where('id', $id)->delete();

        // Cara Eloquent
        Pertanyaan::destroy($id);
        return redirect('/pertanyaan')->with('success', "Post Berhasil dihapus!");
    }
}
