<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FormConroller extends Controller
{
    public function home() {
        return view('home');
    }

    public function register() {
        return view('form');
    }

    public function sapa(Request $request) {
        $nama = $request["namadepan"] . " " . $request["namabelakang"];
        return "<h1>SELAMAT DATANG $nama !</h1>" . view('selamatdatang');
    }
}
